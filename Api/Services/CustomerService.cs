using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Services.Interfaces;
using Api.Repositories.Interfaces;
using Entities;

namespace Api.Services
{
    public class CustomerService: ICustomerService
    {
        private readonly ICustomerRepository _icustomrepository;
        public CustomerService(ICustomerRepository icustomrepository){
            _icustomrepository = icustomrepository;
        }
        public async Task<List<Customer>> GetAllAsync()
        {
            return await _icustomrepository.GetAllAsync();
        }
    }
}