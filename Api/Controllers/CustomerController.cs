using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entities;
using Api.Responses;
using Api.Services.Interfaces;

namespace Api.Controllers
{
    [ApiController]
    public class CustomerController:ControllerBase
    {

        private readonly ICustomerService _customerservice;

        public CustomerController(ICustomerService customerService)
        {
            _customerservice = customerService;
    
        }

        [HttpGet]
        [Route("api/[controller]")]
        public async Task<ActionResult<Response<IEnumerable<Customer>>>> GetAll()
        {
        
        var response = new Response<IEnumerable<Customer>>();
        var customer = await _customerservice.GetAllAsync();
        response.Data = customer;

        return Ok(response);
        /*var response = new Response<IEnumerable<Customer>>();
        var lst = new List<Customer>();
        var customer = new Customer{Id = 1};

        lst.Add(customer);

        response.Data = lst;

        return response;*/
        }
    }
}