import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';

import App from './app'; 

let x = 1

const app = 
//Fragment es similar a div
<Fragment>
  <h1>Hola mundo {x} </h1>
  <h2>Hola mundo 2</h2>
</Fragment>


ReactDOM.render(<App title='Titulo' text='Texto'/>, document.getElementById('root'))

